# Report Generator

> Report Generator is a project for internship selection organized by **YoungInnovations**
> Project is hosted on heroku as per the guidelines
> https://report-generator-intern.herokuapp.com/
> Programming Language used: **PHP**

## Request Flow

---

Code is written in a such way that, it will find min,max,avg.

- **autoloader.php** includes all the classes inside **app** folder
- A new instance of class **ReportGenerator** is instantiated which extends class **DbHandler**(it handles all the database logic).
- ReportGenerator calls its parent's constructor(DbHandler) once instantiated through its constructor
- DbHandler extends another abstract class **AbstractSQLiteConn** which only establishes a connection to SQLite and sets its path.
- DbHandler's constructor initiates the connection to the database, gets the JSON from the provided endpoint, and sets it to the respective variable.
- ReportGenerator creates all three tables and populates it with the data in a normalized form.
- from the instance of class ReportGenerator in index.php, method name output is called which processes the data from the SQLite database to the desired output as per requirement.

> _Note that this was my first time performing unit testing. So I had to use **PHPUnit** to perform the task_.

I did not plan to use any extra dependency or package on this project but doing further research I found most of the PHP developers use PHPUnit which is an easy, well-described, and yet powerful tool. So to compensate that I did not use composer(package/dependency manager for PHP) to install PHPUnit but instead I used **PHAR**(PHP Archive file type) to install PHPUnit globally in my system by following the instructions below

https://phpunit.de/manual/6.5/en/installation.html

> I used PHPUnit 9.3.8 which is compatible with the PHP version that I use PHP 7.4.9. On more info on compatibility we can checkout https://phpunit.de/supported-versions.html

We can use composer(I tried both the method) to install PHPUnit which is quite easy but be sure to not run on compatibility issue like so:
![Scheme](https://i.imgur.com/W4GotM8.jpg)

## Database

---

As per the guidelines I used SQLite. **PHP Data Objects(PDO)** was used to interact with the database. I had to enable PHP_SQLITE driver from php.ini(uncomment the extension=php_sqlite from php.ini).

## Heroku

---

But hosting on **Heroku** was quite different. I had to use composer to enable PHP_SQLITE driver. Thanks to https://stackoverflow.com/questions/37386926/deploying-laravel-on-heroku-pdoexception-in-connector-php-line-55-sqlite article on stackoverflow.

composer.json:

```
{
  "name": "swastik/intern-heroku",
  "authors": [
    {
      "name": "myUserName007",
      "email": "my.email@gmail.com"
    }
  ],
  "require": {
    "ext-pdo_sqlite": "*"
  }
}

```

## Autoloading

---

I created autoloader.php to autoload all the files present inside the app folder. which made me not to worry about 'include or include_once'.

## Folder Structure

---

```

├─ app
│  ├─ AbstractSQLiteConn.php
│  ├─ config
│  │  └─ Config.php
│  ├─ DbHandler.php
│  └─ Rectify.php
├─ db
│  └─ phpsqlite.db
├─ tests
│  └─ unit
│     ├─ DbTest.php
│     ├─ ReportGeneratorTest.php
│     └─ SampleTest.php
├─ .gitignore
├─ autoloader.php
├─ index.php
├─ phpunit.xml
├─ readme.md
├─ ReportGenerator.php
└─ yipl-logo.png

```

## Bonus Points

---

All bonus points are accomplished

- Normalize the data and store into relational structure. :heavy_check_mark:
- Fetch the data from the newly stored sqlite database .:heavy_check_mark:
- Write and include unit tests for your code. :heavy_check_mark:
- Write readme files with the instructions necessary to run the code. :heavy_check_mark:

## Other References

---

- [CodeCourse](https://www.youtube.com/user/phpacademy) for basics of PHPUnit.
- [PHPUnit](https://phpunit.readthedocs.io/en/9.3/) Manual
- [PHP Offical website](https://www.php.net/) for utility functions
- [StackOverflow]("www.stackoverflow.com) for tons of solutions which I have mentioned in the code itself.

---

Thanks to **YoungInnovations** that I learned unit testing by using PHPUnit which was new for me and also for providing this challenging task.

Submitted by Swastik Thapaliya :smiley: Project hosted on **[Heroku](https://report-generator-intern.herokuapp.com/)**.
