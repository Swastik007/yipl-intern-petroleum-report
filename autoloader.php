<?php

/**
 * Autoloader loads every file located on the app folder so we don't have to include every file specifically each time.
 * Reference taken from
 * https://stackoverflow.com/questions/48895926/how-to-use-file-exists-with-autoload
 */

spl_autoload_register(function ($class) {

    $prefixes = ['config', ''];

    foreach ($prefixes as $prefix) {
        if (file_exists("app/$prefix/$class.php")) {
            include_once "app/$prefix/$class.php";
        }

    }
});