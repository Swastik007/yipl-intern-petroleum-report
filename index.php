<?php
include_once 'autoloader.php';
include_once 'ReportGenerator.php';

$reportGenerator = new ReportGenerator();
$results = $reportGenerator->output();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ReportGenerator</title>
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container mt-5">

        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="yipl-logo.png" alt="" width="20%" height="50%">
            <h2>Report Generator For Internship Selection</h2>
            <p class="lead">Submitted by <a href="https://www.linkedin.com/in/swastik-thapaliya-614338121/">Swastik
                    Thapaliya</a> &#128513;</p>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product</th>
                    <th scope="col">Year</th>
                    <th scope="col">Min</th>
                    <th scope="col">Max</th>
                    <th scope="col">Avg</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $key => $result): ?>
                <tr>
                    <th scope="row"><?php echo ++$key; ?></th>
                    <td><?php echo $result['petroleum_product'] ?></td>
                    <td><?php echo $result['year'] ?></td>
                    <td><?php echo $result['min'] ?></td>
                    <td><?php echo $result['max'] ?></td>
                    <td><?php echo $result['avg'] ?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>

</body>

</html>