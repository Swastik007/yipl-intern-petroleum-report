<?php

class ReportGenerator extends DbHandler
{
    public function __construct()
    {
        parent::__construct();

        /**
         * this is a automation
         * which checks if connection exists
         * creates all the required table
         * populates the table with filtered data.
         */

        if ($this->connectionCheck()) {
            $this->createTables();
            $this->populate();
        }
    }

    /**
     * Rectify is the main class where logic is placed to get the required output
     */
    public function output()
    {
        $_rectifiedVariables = new Rectify();
        /**
         * reindex is callable function passed from the parent class DbHandler
         */
        $this->rectifiedVariables = $_rectifiedVariables->refinedData($this->normalizeDataFromDB(), [$this, 'reindex']);
        return $this->rectifiedVariables;
    }

}