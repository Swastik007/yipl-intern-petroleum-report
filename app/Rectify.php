<?php
class Rectify
{
    private $commonProductArr = [];
    private $finalArr = [];
    private $_finalArr = [];
    private $salesArr = [];
    private $yearsArr = [];

    public function refinedData(array $unrefinedArr, callable $reindex): array
    {

        while (!empty($unrefinedArr)) {
            /***
             * $name gets the name of the petroleum product from the $unrefinedArr(our main unrefined variables are here) array having 0 index
             * foreach loop after that separates common petroleum product that matches the name in $name variable, pushing it to $commonProductArr
             * and unsets every index that matched from $unrefinedArr
             */
            $name = $unrefinedArr[0]['petroleum_product'];
            foreach ($unrefinedArr as $key => $foo) {
                if ($foo['petroleum_product'] == $name) {
                    array_push($this->commonProductArr, $foo);
                    unset($unrefinedArr[$key]);
                }
            }
            $this->arrange();
            /**
             * reindexing the array because unsetting does not rebase the index
             * it is a callable function that is passed from the class where it has been called
             */
            $unrefinedArr = $reindex($unrefinedArr);
        }

        return $this->finalArr;

    }

    /**
     * this method is the heart of this class because it filters all the common petroleum product
     * and places them to be as required output
     */
    private function arrange()
    {

        /**
         * more description for this method above its definition
         */
        $this->chunker(5);

        foreach ($this->commonProductArr as $bar) {

            foreach ($bar as $baz) {
                $this->_finalArr['petroleum_product'] = $baz['petroleum_product'];
                if ($baz['sale'] != 0) {
                    array_push($this->salesArr, $baz['sale']);
                }
                array_push($this->yearsArr, $baz['year']);
            }

            $this->_finalArr['year'] = $this->getYearsRange();
            $this->_finalArr['max'] = $this->getMaxSales();
            $this->_finalArr['min'] = $this->getMinSales();
            $this->_finalArr['avg'] = $this->getAvgSales();

            array_push($this->finalArr, $this->_finalArr);
            $this->resetArray();
        }
    }

    /**
     * this method chunks the array into given size
     * for this short project as per requirement it is placed 5
     */
    private function chunker(int $size): void
    {
        if ($size > sizeof($this->commonProductArr)) {
            throw new Exception("Chunk size must be less than the size of actual filtered array ");
        }
        $this->commonProductArr = array_chunk($this->commonProductArr, $size);
    }

    private function resetArray(): void
    {
        $this->salesArr = [];
        $this->yearsArr = [];
        $this->commonProductArr = [];
        $this->_finalArr = [];
    }

    private function checkIfArrayIsNotEmpty(array $arr): bool
    {
        return (!empty($arr)) ? true : false;
    }

    private function getYearsRange(): string
    {
        return min($this->yearsArr) . '-' . max($this->yearsArr);
    }

    private function getMaxSales(): string
    {
        return $this->checkIfArrayIsNotEmpty($this->salesArr) ? max($this->salesArr) : 0;
    }

    private function getMinSales(): string
    {
        return $this->checkIfArrayIsNotEmpty($this->salesArr) ? min($this->salesArr) : 0;
    }

    private function getAvgSales(): string
    {
        return $this->checkIfArrayIsNotEmpty($this->salesArr) ? array_sum($this->salesArr) / count($this->salesArr) : 0;
    }

}