<?php
class Config
{
    /**
     * path to the sqlite
     * Reference taken from
     * https://www.sqlitetutorial.net/sqlite-php/connect/
     */
    const PATH_TO_SQLITE_FILE = 'db/phpsqlite.db';

    const PATH_TO_JSON_DATA = 'https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json';

}