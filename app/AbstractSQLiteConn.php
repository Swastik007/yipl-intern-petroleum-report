<?php
/**
 * Reference taken from
 * https://www.sqlitetutorial.net/sqlite-php/connect/
 */

abstract class AbstractSQLiteConn
{
    /**
     * PDO instance
     * @var type
     */
    protected $pdo;

    /**
     * configuration for pdo driver
     * throws exception incase of error
     * fetches data in associative array by default
     */
    private $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];

    /**
     * return in instance of the PDO object that connects to the SQLite database
     * @return \PDO
     */
    public function connect()
    {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . Config::PATH_TO_SQLITE_FILE, '', '', $this->options); //Config is a constant so we can directly call it like so.
        }
    }
}