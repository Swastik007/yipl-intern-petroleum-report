<?php

class DbHandler extends AbstractSQLiteConn
{

    protected $rawVariables;
    protected $normalizeVariables;

    public function __construct()
    {
        $this->connect(); //Method from AbstractSQLiteConn
        $this->setRawVariables(); // Method to read data from report.json and set it to $rawVariables
    }

    public function setRawVariables()
    {
        /**
         * Config::PATH_TO_JSON_DATA holds the endpoint for API
         * https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json
         */

        $this->rawVariables = json_decode(file_get_contents(Config::PATH_TO_JSON_DATA), true);
    }

    /**
     * Reference from https://www.sqlitetutorial.net/sqlite-php/create-tables/
     * @desc create normalized table
     */
    public function createTables(): void
    {

        try {
            $commands = [
                'CREATE TABLE IF NOT EXISTS year_of_sale(
                id INTEGER PRIMARY KEY NOT NULL,
                title INTEGER UNIQUE
            )',
                'CREATE TABLE IF NOT EXISTS petroleum_product(
                id INTEGER PRIMARY KEY  NOT NULL,
                title VARCHAR(255) UNIQUE
            )',

                'CREATE TABLE IF NOT EXISTS sales_report(
                id INTEGER PRIMARY KEY  NOT NULL,
                petroleum_product INTEGER,
                year_of_sale INTEGER,
                sales_amount INTEGER,
                FOREIGN KEY (petroleum_product)
                REFERENCES petroleum_product(id) ON UPDATE CASCADE
                ON DELETE CASCADE,
                FOREIGN KEY (year_of_sale)
                REFERENCES year_of_sale(id) ON UPDATE CASCADE
                ON DELETE CASCADE
            )',
            ];

            // execute the sql commands to create new tables
            foreach ($commands as $command) {
                $this->pdo->exec($command);
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * populate each of the table
     */

    public function populate(): bool
    {
        $years = $this->getSpecificValue('year', true);
        $petroleumProducts = $this->getSpecificValue('petroleum_product', true);

        if (empty($this->getData('year_of_sale'))) {

            $this->yearAndProductInsertion('year_of_sale', $years);
        }

        if (empty($this->getData('petroleum_product'))) {
            $this->yearAndProductInsertion('petroleum_product', $petroleumProducts);
        }

        $this->normalizeTheData();

        if (empty($this->getData('sales_report', true))) {
            $this->salesReportInsertion();
        }

        return true;
    }

    public function normalizeDataFromDB(): array
    {
        $stmt = $this->pdo->query("SELECT p.title AS 'petroleum_product', y.title AS 'year_of_sale', s.sales_amount AS 'sales_amount' FROM sales_report AS s
                    INNER JOIN petroleum_product AS p ON p.id = s.petroleum_product
                    INNER JOIN year_of_sale AS y on y.id = s.year_of_sale");

        $data = [];
        while ($row = $stmt->fetch()) {
            $data[] = [
                'petroleum_product' => $row['petroleum_product'],
                'year' => $row['year_of_sale'],
                'sale' => $row['sales_amount'],
            ];
        }
        return $data;
    }

    /**
     * Reference from
     * https://www.sqlitetutorial.net/sqlite-php/query/
     * get data from the tables
     * if $salesReportTable is set to true then it gets data from sales_report table(which have to be passed to $table or
     * else method will throw an error) because it has different
     * structure
     */

    public function getData(string $table, bool $salesReportTable = false): array
    {
        $stmt = $this->pdo->query('SELECT * from ' . $table);
        $data = [];

        if (!$salesReportTable) {
            while ($row = $stmt->fetch()) {
                $data[] = [
                    'id' => $row['id'],
                    'title' => $row['title'],
                ];
            }
        } else {
            while ($row = $stmt->fetch()) {
                $data[] = [
                    'id' => $row['id'],
                    'petroleum_product' => $row['petroleum_product'],
                    'year_of_sale' => $row['year_of_sale'],
                    'sales_amount' => $row['sales_amount'],
                ];
            }
        }

        return $data;
    }

    /**
     * Drops every table
     */

    public function droptables()
    {
        $tableNames = $this->getTableNames();
        foreach ($tableNames as $tableName) {
            $stmt = $this->pdo->query('DROP TABLE IF EXISTS ' . $tableName);
            $stmt->execute();
        }

    }

    /**
     * Insert normalized data into sales_report table
     */

    public function salesReportInsertion(): bool
    {
        $sql = "INSERT INTO sales_report(petroleum_product,year_of_sale,sales_amount) VALUES (:petroleum_product,:year_of_sale,:sales_amount)";
        $stmt = $this->pdo->prepare($sql);

        foreach ($this->normalizeVariables as $result) {
            $stmt->bindValue(':petroleum_product', $result['petroleum_product']);
            $stmt->bindValue(':year_of_sale', $result['year']);
            $stmt->bindValue(':sales_amount', $result['sale']);
            $stmt->execute();
        }

        return true;

    }

    /**
     * Insert a years into the year_of_sale table
     * Insert a Petroleum Product into the petroleum_product table
     */
    public function yearAndProductInsertion(string $table, array $values): bool
    {

        $sql = "INSERT INTO " . $table . "(title) VALUES (:value)";
        $stmt = $this->pdo->prepare($sql);

        foreach ($values as $value) {
            $stmt->bindValue(':value', $value);
            $stmt->execute();
        }

        return true;
    }

    /**
     * Helper methods below
     */

    /**
     * replace product name and year with their respective id from $rawVariable
     * ahd pass it to $normalizeVariables
     */

    public function normalizeTheData(): void
    {
        $_rawVariables = $this->rawVariables;

        $years = $this->getData('year_of_sale');
        $petroleumProducts = $this->getData('petroleum_product');

        foreach ($_rawVariables as $key => $_rawVariable) {
            foreach ($years as $year) {
                if ($year['title'] == $_rawVariable['year']) {
                    $_rawVariables[$key]['year'] = $year['id'];
                }
            }
            foreach ($petroleumProducts as $petroleumProduct) {
                if ($petroleumProduct['title'] == $_rawVariable['petroleum_product']) {
                    $_rawVariables[$key]['petroleum_product'] = $petroleumProduct['id'];
                }
            }
        }

        $this->normalizeVariables = $_rawVariables;

    }

    public function getNormalizeData(): array
    {
        return $this->normalizeVariables;
    }

    public function getRawData(): array
    {
        return $this->rawVariables;
    }

    /**
     * * get specfic value from the $rawVariable
     * example:
     * if you pass $key = 'year' it would get only value of year from $rawVariable
     * $cleanese is a option to make particular array unique, re-index it and sort it
     */
    public function getSpecificValue(string $key, bool $cleanese = false): array
    {
        $tempArr = [];

        foreach ($this->rawVariables as $rawVariable) {
            array_push($tempArr, $rawVariable[$key]);
        }

        if ($cleanese) {
            $tempArr = array_unique($tempArr);
            $tempArr = $this->reindex($tempArr);
            sort($tempArr);
        }

        return $tempArr;
    }

    /**
     * Reference from https://www.sqlitetutorial.net/sqlite-php/create-tables/
     * get the table list in the database
     * This method is used for test case to check whether database contains expected three tables
     */
    public function getTableNames(): array
    {

        $stmt = $this->pdo->query("SELECT name
                                   FROM sqlite_master
                                   WHERE type = 'table'
                                   ORDER BY name");
        $tables = [];
        while ($row = $stmt->fetch()) {
            $tables[] = $row['name'];
        }

        return $tables;
    }

    //reindexes the array
    public function reindex($the_array): array
    {
        $temp = $the_array;
        $the_array = array();
        foreach ($temp as $value) {
            $the_array[] = $value;
        }
        return $the_array;
    }

    /**
     * check if connection has been established
     */
    public function connectionCheck(): bool
    {
        return ($this->pdo) ? true : false;
    }

}