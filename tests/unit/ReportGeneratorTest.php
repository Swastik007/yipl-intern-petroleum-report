<?php

include_once 'ReportGenerator.php';

use \PHPUnit\Framework\TestCase as TestCase;

class ReportGeneratorTest extends TestCase
{
    public function setUp(): void
    {
        $this->reportGenerator = new ReportGenerator();
    }

    /**
     * @test
     */
    public function finalOutput()
    {
        $data = $this->reportGenerator->output();
        $this->assertTrue(true);
    }

    public function tearDown(): void
    {
        $this->reportGenerator->dropTables();
    }

}