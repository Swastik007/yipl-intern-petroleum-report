<?php

use \PHPUnit\Framework\TestCase as TestCase;

class DbTest extends TestCase
{

    protected $dbHandler;

    public function setUp(): void
    {
        $this->dbHandler = new DbHandler();
    }

/**
 * @test
 */
    public function connectionCheck()
    {
        $this->assertTrue($this->dbHandler->connectionCheck());
    }
/**
 * @test
 */
    public function getDataFromAPIEndpoint()
    {
        $data = json_decode(file_get_contents(Config::PATH_TO_JSON_DATA), true);
        $this->assertTrue(!empty($data));

    }

/**
 * @test
 */
    public function createTable()
    {
        $this->dbHandler->createTables();
        $this->assertTrue(true);
    }
/**
 * @test
 */
    public function getTableNames()
    {
        $expectedTables = ['petroleum_product', 'sales_report', 'year_of_sale'];
        $returnedTables = $this->dbHandler->getTableNames();
        $this->assertEquals($expectedTables, $returnedTables);
    }

    /**
     * @test
     */
    public function getYear()
    {
        /**
         * without setting cleanse to true which returns all of the year of sale
         */

        $expectedYears = $this->dbHandler->getSpecificValue('year');
        $expectedYears = array_unique($expectedYears);
        $expectedYears = $this->dbHandler->reindex($expectedYears);
        sort($expectedYears);

        $returnedYears = $this->dbHandler->getSpecificValue('year', true);
        $this->assertEquals($expectedYears, $returnedYears);

    }

    /**
     * @test
     */
    public function getPetroleumProduct()
    {
        /**
         * without setting cleanse to true which returns all of the petroleum product
         */
        $expectedPetroleumProducts = $this->dbHandler->getSpecificValue('petroleum_product');
        $expectedPetroleumProducts = array_unique($expectedPetroleumProducts);
        $expectedPetroleumProducts = $this->dbHandler->reindex($expectedPetroleumProducts);
        sort($expectedPetroleumProducts);

        $returnedPetroleumProducts = $this->dbHandler->getSpecificValue('petroleum_product', true);
        $this->assertEquals($expectedPetroleumProducts, $returnedPetroleumProducts);

    }

    /**
     * @test
     */
    public function getYearFromDB()
    {
        $data = $this->dbHandler->getData('year_of_sale');
        //var_dump($data);
        //die(); //use this to check for data output
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function getPetroleumProductFromDB()
    {
        $data = $this->dbHandler->getData('petroleum_product');
        //var_dump($data);
        //die(); //use this to check for data output
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function getNormalizeData()
    {
        $this->dbHandler->normalizeTheData();
        $data = $this->dbHandler->getNormalizeData();
        // var_dump($data);
        // die(); //use this to check for data output
        $this->assertTrue(true);
    }

    /**
     * @test
     */

    public function populateDB()
    {
        $this->assertTrue($this->dbHandler->populate());

    }

    /**
     * @test
     */
    public function getNormalizeDataFromDB()
    {
        $finalResult = $this->dbHandler->normalizeDataFromDB();
        $expectedResult = $this->dbHandler->getRawData();
        $this->assertEquals($finalResult, $expectedResult);
    }

    /**
     * @test
     * drops all the table after the test
     */

    public function dropTable()
    {
        $this->dbHandler->dropTables();
        $this->assertTrue(true);
    }

}